import { PubSub } from 'graphql-subscriptions'

/* Example

    pubsub.publish('fooChanged', {
        fooChanged: {
            id: 1,
            content: 'Hello!'
        }
    })
*/

export default new PubSub