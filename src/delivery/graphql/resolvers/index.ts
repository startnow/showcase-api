import CommentResolvers from './comment'


export default {
    Query: {
        ...CommentResolvers.Query,
    },
    Mutation: {
        ...CommentResolvers.Mutation,
    },
    Subscription: {
        ...CommentResolvers.Subscription,
    },
}