// Apollo
import { withFilter } from 'graphql-subscriptions'
import pubsub from './pubsub'

// Utils
import * as shortid from 'shortid'


const comments = [
    { content: 'Initial comment !', author: 'Raphael', id: shortid.generate() },
];

class Resolver {

    static comments(_, args) {
        return comments;
    }

    static addComment(_, { input }) {

        const { author, content } = input;
        const commentAdded = {
            author, content, id: shortid.generate(),
        };
        
        comments.push(commentAdded);
        
        pubsub.publish('commentAdded', { commentAdded });
        
        return { comment: commentAdded };
    }

    static subscribeCommentAdded(_, args, context, info) {
        return pubsub.asyncIterator(['commentAdded']);
    }

    static filterCommentAdded(payload, variables) {
        return payload.commentAdded.author === variables.author;
    }
}

export default {
    Query: {
        comments: Resolver.comments,
    },
    Mutation: {
        addComment: Resolver.addComment,
    },
    Subscription: {
        commentAdded: {
            subscribe: withFilter(
                Resolver.subscribeCommentAdded,
                Resolver.filterCommentAdded,
            ),
        },
    },
}