export default `
    type Comment {
        content: String
        author: String
        id: ID!
    }

    extend type Subscription {
        commentAdded(
            author: String!
        ): Comment!
    }

    extend type Query {
        comments: [Comment]
    }

    input AddCommentInput {
        content: String!
        author: String!
    }

    type AddCommentPayload {
        comment: Comment!
    }
    
    extend type Mutation {
        addComment(
            input: AddCommentInput!
        ): AddCommentPayload!
    }
`