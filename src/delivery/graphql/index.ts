// Server
import { ApolloServer } from 'apollo-server-express'
import { Application } from 'express'
import { Server } from 'http'

// GraphQL
import resolvers from './resolvers'
import typeDefs from './definitions'

// Helpers
import { ensureAuth } from './helpers'


const onConnect = connectionParams => {
    console.log('New subscriber connected!');

    return null;

    // if (connectionParams.hasOwnProperty('Authorization')) {
    //     const { Authorization: authorization } = connectionParams;
    //     return ensureAuth({ headers: { authorization } });
    // }

    // return ensureAuth(connectionParams);
}

const context = async ({ req, connection }) => {
    // connection is defined when using websocket
    // while req when using express.
    return req || connection.context;
}

export default (app: Application, server: Server) => {
    
    // Create instance
    const instance = new ApolloServer({
        typeDefs, resolvers, context,
        subscriptions: { onConnect },
    });
    
    // Configure
    instance.applyMiddleware({ app });
    instance.installSubscriptionHandlers(server);
    
    return instance;
}
