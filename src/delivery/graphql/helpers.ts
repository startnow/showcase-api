export const ensureAuth = ({ headers }) => {

    let { authorization } = headers;

    if (!authorization) return { user: undefined };

    // Extract Authorization
    // const matchs = /Bearer (?<token>.*)/i.exec(authorization);
    const matchs = (authorization as string).match(/Bearer (.*)/i);

    if (!matchs) throw new Error('Missing auth token!');

    // Decode the payload
    // const { token } = matchs.groups;
    const token = matchs[1];

    const payload = {
        /* @TODO: decodeToken */
    }

    return { user: payload || undefined };
}