// Apollo
import createApolloServer from './graphql'

// Express
import { json, urlencoded } from 'body-parser'
import { createServer } from 'http'
import * as express from 'express'


const PORT = 3000;
const app = express();
const server = createServer(app);

app.use(json());
app.use(urlencoded({
    extended: false,
}));

const apollo = createApolloServer(app, server);

server.listen(PORT, '0.0.0.0', () => {
    console.log(`Starting SERVER on port ${PORT}`);
});